import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  nbRandom = 0;
  score = 0;
  showGame = false;
  showReplay = false;

  constructor() { }

  ngOnInit(): void {
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  start() {
    this.nbRandom = this.getRandomInt(100)
    console.log(this.nbRandom);
    this.score = 0;
    this.showGame = true;
    this.showReplay = false;
  }

  envoye(nombre) {
    this.score += 1;
    if(nombre < this.nbRandom){
      window.alert("C'est plus !");
    } else if (nombre > this.nbRandom){
      window.alert("C'est moins !");
    } else {
      window.alert("Bravo, votre score est de " + this.score + " points");
      this.showReplay = true;
    }
  }

}
